import time # used to get timestamp
import thingspeak
from gpiozero import MCP3008
from functions import *

# define functions
def raw_to_voltage(raw_val):
    result = raw_val*(3.3/1024)
    return result

def now_ms():
    ms = int(time.time() * 1000)
    return ms

# initialize variables
adc = MCP3008(channel=0, clock_pin=11, mosi_pin=10, miso_pin=9, select_pin=8)
loop_duration = 15000 # in milliseconds
loop_start = now_ms()
channel_id = get_key('channel.txt')  # PUT YOUR CHANNEL ID IN A FILE CALLED channel.txt LOCATED IN THE SAME FOLDER AS main.py
write_key = get_key('write_key.txt') #PUT YOUR API WRITE KEY IN A FILE CALLED wrirte_key.txt LOCATED IN THE SAME FOLDER AS main.py

# initialize the connection to thingspeak
channel = thingspeak.Channel(id=channel_id, api_key=write_key)

# loop for sending values to thinkspeak
while True:
    if (now_ms() - loop_start > loop_duration):
        # send values to Thingspeak here
        channel.update({1: raw_to_voltage(adc.raw_value)})
        channel.update({2: adc.raw_value})
        time.sleep(0.5)
        loop_start = now_ms()
        continue # continue to top of while loop
    else:
        #reads potentiometer and updates it here
        channel.update({1: raw_to_voltage(adc.raw_value)})
        channel.update({2: adc.raw_value})
        time.sleep(1) # simulates some operations that takes 1 second (remove in live system)
        print(f'loop elapsed ms {now_ms() - loop_start}') # print loop time
