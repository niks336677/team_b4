Recreation checklist for team <Team_B1>
===================================================

Team name: <Team_B4>

Checklist
------------

- [ ] Project plan completed
- [ ] System block diagram completed
- [ ] Recreate documentation clear and understandable
- [ ] ADC communicating with RPi
- [ ] Temperature sensor communicating with RPi
- [ ] BTN/LED board communicating with RPi
- [ ] Data from RPi sent to Thingspeak


Comments
-----------

<add a bulleted list of bug issues you have created on the projects of other team's>

The only part which we can be recreated it's the wiring, but we can't do anything else because the guide doesn't make any sense and we don't have the code.

<Any comments that may be relevant for you, other team's or teachers. (Could be that something is super successfull)>