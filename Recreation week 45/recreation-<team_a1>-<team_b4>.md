Recreation checklist for team <Team A1>
===================================================

Team name: <team_b4>

Checklist
------------

- [x] Project plan completed
- [x] System block diagram completed
- [x] Recreate documentation clear and understandable
- [x] ADC communicating with RPi
- [x] Temperature sensor communicating with RPi
- [x] BTN/LED board communicating with RPi
- [x] Data from RPi sent to Thingspeak


Comments
-----------

<add a bulleted list of bug issues you have created on the projects of other team's>

The wires from the Fritzing schematic are done weirdly by the app, and also somethings are not the same as the real materials.

<Any comments that may be relevant for you, other team's or teachers. (Could be that something is super successfull)>